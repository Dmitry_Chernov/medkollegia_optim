<?php


?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="yandex-verification" content="840b5c7b1f20df88" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				w.yaCounter23840071 = new Ya.Metrika({
					id:23840071,
					clickmap:true,
					trackLinks:true,
					accurateTrackBounce:true,
					webvisor:true
				});
			} catch(e) { }
		});

		var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
	})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/23840071" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/assets/images/favicon.ico" >
</head>
<!-- /Yandex.Metrika counter -->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'medical-theme' ); ?></a>

	<header id="header" class="head">
		<div class="top-header">
			<div class="container">
				<div class="row ">
					<ul class="contact-detail2 col-md-6 pull-left">
						<li > <a href="tel:+74956422932#" rel="nofollow" ><i class="fa fa-phone-square"></i> + 7 (495) 642 29 32</a> </li>
						<li> <a href="mailto:info@medkollegia.com?subject=Вопрос по медколлегии" target="_blank"><i class="fa fa-envelope-o"></i> help@medkollegia.com</a> </li>
					</ul>
					<div class="social-links col-md-6 pull-right">
						<ul class="social-icons pull-right hidden-xs">
							<li> <a href="http://medkollegia.com" target="_blank"><i class="fa fa-facebook"></i></a> </li>
							<li> <a href="http://medkollegia.com" target="_blank"><i class="fa fa-twitter"></i></a> </li>
							<li> <a href="http://medkollegia.com" target="_blank"><i class="fa fa-pinterest"></i></a> </li>
							<li> <a href="http://medkollegia.com" target="_blank"><i class="fa fa-skype"></i></a> </li>
							<li> <a href="http://medkollegia.com" target="_blank"><i class="fa fa-dribbble"></i></a> </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-default navbar-menu">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Навигация по сайту</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo home_url(); ?>">
						<div class="logo-text pull-right  hidden-sm"><span class="red">Мед</span><small>коллегия</small></div>
						 <img  class="img-responsive logo pull-left hidden-md hidden-sm" src="<?php echo get_template_directory_uri();?>/assets/images/heart_logo.png" alt="logo">
					</a>
			</div>
				<?php
				wp_nav_menu( array(
					'menu'            => 'primary',
					'theme_location'  => 'primary',
					'depth'           => 2,
					'container'       => 'div',
					'container_class' => 'navbar-collapse collapse',
					'container_id'    => 'bs-example-navbar-collapse-1',
					'menu_class'      => 'nav navbar-nav navbar-right text-uppercase',
					'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
					'walker'          => new wp_bootstrap_navwalker()

				));

				?>
			</div>
		</nav>
	</header>

	<div id="content" class="site-content">
		<?php if ( !is_home() && !is_front_page() ) : ?>
		<section id="inner-title" class="inner-title">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-lg-6">
						<h2><?php is_page() ? the_title() : single_cat_title();  ?></h2>
					</div>
					<div class="col-md-6 col-lg-6">
						<div class="breadcrumbs">
							<ul>
								<?php
								if(function_exists('bcn_display') && !is_front_page() )
								{
									if(!is_page()){bcn_display_list();}
								}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>

		<? endif; ?>