<form role="search" method="get" id="searchform" class="searchform search-form" action="<?php echo home_url('/'); ?>">
    <input type="text" class="blog-search-field" placeholder="Поиск по сайту" name="s" id="search" value="<?php the_search_query(); ?>">
    <button type="submit">
        <i class="fa fa-search"></i>
    </button>
</form>