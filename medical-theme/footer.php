<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Medicamp
 */

?>

	</div><!-- #content -->

<section id="footer-top" class="footer-top">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-lg-3">
				<div class="footer-top-box">
					<h4>О нас</h4>
					<p>«Медколлегия»</br>это сообщество врачей-профессионалов своего дела, направленное на успешную диагностику и лечение заболеваний.</p>
				</div>
				<div class="footer-top-box">
					<h4>График работы</h4>
					<b>Пон-Пятн :</b> с 9:00 до 21:00<br/>
					<b>Суббота:</b> c 9:00 до 20:00</br>
					<b>Воскресенье:</b> c 9:00 до 16:00</br>
				</div>
			</div>
			<div class="col-md-3 col-lg-3">
				<div class="footer-top-box">
					<h4>Последние статьи</h4>
					<ul>
						<?php
						$args = array(
							'numberposts' => 3,
						    'post_status' => 'publish'
						);

						$result = wp_get_recent_posts($args);

						foreach( $result as $post ){
						?>
						<li>
							<div class="recent-post-widget">
								<a href="<?php echo get_permalink( $post['ID'] ) ?>" class="widget-img-thumb">
									<?php echo get_the_post_thumbnail($post['ID'], 'thumbnail'); ?>
								</a>
								<div class="widget-content">
									<h5><a href="<?php echo get_permalink( $post['ID'] ); ?>" class="sidebar-item-title"><?php echo $post['post_title']; ?></a></h5>
									<a href="<?php echo the_permalink(); ?>">
										<p class="widget-date"><?php echo $post['post_date']; ?></p>
									</a>
								</div>
								<div class="clearfix"></div>
							</div>
						</li>
							<?php
						}
						?>
					</ul>
				</div>
			</div>
			<div class="col-md-3 col-lg-3">
				<div class="footer-top-box">
					<h4>Заметки</h4>
					<?php
					$tags = get_tags($args);

					foreach( $tags as $tag ) {

						$tag_link = get_tag_link($tag->term_id);
						?>

					<div class="tagcloud"><a href="<?php echo $tag_link; ?>" title="<?php echo $tag->name; ?>" ><?php echo $tag->name; ?></a></div>
					<?php } ?>

				</div>
			</div>
			<div class="col-md-3 col-lg-3">
				<div class="footer-top-box">
					<h4>Контакты</h4>
					<p><b>Москва : Болотниковская 36 корпус 6<br/>
						<b>Тел:<a href="tel:+74956422932#" rel="nofollow" class="red" > +7 (495) 642 29 32</a><br/>
						<b>Mail: </b> help@medkollegia.com </p>
				</div>
				<div class="footer-top-box">
					<h4>Рассылка</h4>
					<div class="cs-form">
					<!--Рассылка-->
						<?php if (do_shortcode('[contact-form-7 id="322" title="Форма подписки"]')):
							 echo do_shortcode('[contact-form-7 id="322" title="Форма подписки"]');
						endif; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<p style="font-size: 11px; line-height: 14px;">Опубликованные на сайте материалы, включая информацию о ценах и предложениях клиники, могут отличаться от действующих. Внимание! Вся информация, опубликованная на этом сайте, является справочной или популярной.<br> Диагностика и назначение лекарственных препаратов требуют знания истории болезни и непосредственного обследования врачом. Обращаем ваше внимание на то, что данный интернет-сайт носит исключительно информационный характер и ни при каких условиях не является публичной офертой, определяемой положениями Статьи 437 (2) Гражданского кодекса РФ.<br>
						Лицензия: ЛО-77-01-006035

					</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="footer-bottom" class="footer-bottom">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-lg-9">
				<div class="copyright">Copyright &copy; 2016. All Rights Reserved</div>
			</div>
			<div class="col-lg-3">
				<ul class="list-inline social-buttons">
					<li><a href="<?php echo home_url(); ?>"><i class="fa fa-twitter"></i></a></li>
					<li><a href="<?php echo home_url(); ?>"><i class="fa fa-facebook"></i></a></li>
					<li><a href="<?php echo home_url(); ?>"><i class="fa fa-linkedin"></i></a></li>
					<li><a href="<?php echo home_url(); ?>"><i class="fa fa-youtube"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</section>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
