<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<section id="section14" class="section-margine blog-list">
				<div class="container">
					<div class="row">
						<div class="col-md-9 col-lg-9">
							<?php

								if ( have_posts() ) :

									if ( is_home() && ! is_front_page() ) : ?>
										<header>
											<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
										</header>

									<?php endif; ?>
									<?php while ( have_posts() ) : the_post();
									get_template_part( 'template-parts/content', 'posts' );
									endwhile;
									the_posts_navigation();
									?>
						</div>
						<div class="col-md-3 col-lg-3">
							<?php get_sidebar(); ?>
						</div>

				</div>
			</section>
		<?php
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer();
