<div class="comment-wrap">
	<?php foreach ($comments as $comment){  ?>
		<h4><a href="<?php comment_author_url(); ?>"><?php comment_author(); ?></a> - <small> <?php comment_date(); ?></small></h4>
		<div class="comments-body">
			<p><?php comment_text(); ?></p>
		</div>

	<?php } ?>
</div>

<?php if(comments_open()){ ?>
	<div class="comment-form-container wow fadeInLeft">
          <h4 class="color-deep">Оставьте комментарий:</h4>
          <form  action="<?php echo site_url( 'wp-comments-post.php');?>" method="post" id="commentform" novalidate>
			  <input type="hidden" name="comment_post_ID" value="<?php echo $post->ID; ?>" id="comment_post_ID">
            <div class="col-md-6 nopadding-left">
              <div class="control-group form-group">
                <div class="controls">
                  <input type="text" class="form-control" name="name" id="name" placeholder="Name..">
                  <p class="help-block"></p>
                </div>
              </div>
            </div>
            <div class="col-md-6 nopadding-left">
              <div class="control-group form-group">
                <div class="controls">
                  <input type="tel" class="form-control" name="phone" id="phone"  placeholder="Phone..">
                </div>
              </div>
            </div>
            <div class="col-md-12 nopadding-left">
              <div class="control-group form-group">
                <div class="controls">
                  <input type="email" class="form-control" name="email" id="email"  placeholder="Email..">
                </div>
              </div>
            </div>
            <div class="col-md-12 nopadding-left">
              <div class="control-group form-group">
                <div class="controls">
                  <textarea rows="10" cols="100"  name="comment" class="form-control" id="message" required placeholder="Message.."  maxlength="999" style="resize:none"></textarea>
                </div>
              </div>
            </div>
            <div id="success"></div>
            <!-- For success/fail messages -->
            <div class="col-md-12 nopadding-left">
              <button type="submit" name="submit" class="btn btn-primary">Add a comment</button>
            </div>
          </form>
        </div>
<?php } else { _e('Comment are closed','medical theme'); } ?>


