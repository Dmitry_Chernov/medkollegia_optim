<?php get_header(); ?>
			<section id="section19" class="section19">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-lg-12"><h1>404</h1><h3><?php esc_html_e( 'Упс! Нет такой страницы.', 'medical-theme' ); ?></h3></div>
						<div class="col-md-12 col-lg-12">
							<div class=" text-center">
								<a href="<?php echo home_url(); ?>" class="btn btn-primary">На главную</a>
								<a href="<?php echo home_url('/contacts-page'); ?>" class="btn btn-primary">Сообщить о проблеме</a>
							</div>
						</div>
					</div>
				</div>
			</section>
<?php get_footer();
