<?php get_header(); ?>
<section id="section18" class="section-margine">
    <div class="container">
        <div class="section18">
            <div class="row">
                <div class="col-md-8 col-lg-8 wow fadeInUp">
                    <div class="textcont">
                        <?php the_content(); ?>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 wow fadeInUp" data-wow-delay=".2s">
                    <div class="section-18-img">
                        <?php the_post_thumbnail('full',array('class' => 'img-responsive')); ?>
                    </div>
                </div>
            </div>
            <div class="row mission-vision">
                <div class="col-sm-6 col-md-6 wow fadeInUp" data-wow-delay=".3s">
                    <figure>
                        <img src="<?php echo get_field('about_img_1'); ?>"  class="img-responsive" alt=""/>
                    </figure>
                     <?php the_field('about_1'); ?>
                </div>
                <div class="col-sm-6 col-md-6 wow fadeInUp" data-wow-delay=".4s">
                    <figure>
                        <img src="<?php echo get_field('about_img_2'); ?>"  class="img-responsive" alt=""/>
                    </figure>
                    <?php the_field('about_2'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="section5"  class="section-5 section-margine">
    <div class="container">
        <div class="row my-team">
            <div class="col-md-12">
                <header class="title-head">
                        <h2>Великолепная команда профессионалов</h2>
                        <p>«Медколлегия» это сообщество врачей-профессионалов своего дела, направленное на успешную диагностику и лечение заболеваний.</p>
                        <div class="line-heading">
                            <span class="line-left"></span>
                            <span class="line-middle">+</span>
                            <span class="line-right"></span>
                        </div>
                </header>
            </div>
            <?php $loop = new WP_Query( array( 'post_type' => 'doctor','posts_per_page' => 4 ,'orderby' => 'post_id', 'order' => 'ASC' ) ); ?>
            <?php while( $loop->have_posts() ) : $loop->the_post(); ?>

                <div class="col-md-3 col-sm-6 my-team-member wow fadeInUp">
                    <div class="my-member-img">
                        <?php the_post_thumbnail('full',array('class' => 'img-responsive')); ?>
                    </div>
                    <div class="my-team-detail text-center">
                        <h4 class="my-member-name"><?php echo get_field('doctor_name'); ?></h4>
                        <p class="my-member-post"><?php echo get_field('position'); ?><div class="my-member-social">
                            <ul>
                                <li><a href="<?php echo get_field('email'); ?>" target="_blank"><i class="fa fa-envelope"></i></a></li>
                                <li><a href="<?php echo get_field('facebook'); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="<?php echo get_field('twitter'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="<?php echo get_field('instagram'); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php endwhile; wp_reset_query();?>
        </div>
    </div>
</section>
<section id="section14" class="section-margine">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <header class="title-head">
                    <h2>Последние статьи</h2>
                    <p>Свежие новости и статьи о здоровье</p>
                    <div class="line-heading">
                        <span class="line-left"></span>
                        <span class="line-middle">+</span>
                        <span class="line-right"></span>
                    </div>
                </header>
            </div>
        </div>
        <div class="row">
            <?php
            $args = array( 'posts_per_page' => 3 );
            $query = new WP_Query( $args );
            while ( $query->have_posts() ) {
                $query->the_post(); ?>
                <div class="col-md-4 col-lg-4">
                    <div class="section-14-box wow fadeInUp">
                        <?php if(has_post_thumbnail()) { ?>
                            <?php the_post_thumbnail('full',array('class' => 'img-responsive')); ?>
                        <?php } ?>
                        <h3><a href="<?php the_permalink();?>" title=""><?php the_title();?></a></h3>
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="comments">
                                    <a class="btn btn-primary btn-sm"><?php the_time('H:m M Y'); ?></a>
                                    <a href="<?php the_permalink(); ?>" class="btn btn-primary btn-sm text-uppercase">Читать</a>
                                </div>
                            </div>
                        </div>
                        <p><?php the_excerpt(); ?></p>
                    </div>
                </div>
            <?php }
            wp_reset_postdata(); ?>
        </div>
    </div>
</section>

<?php get_footer();
