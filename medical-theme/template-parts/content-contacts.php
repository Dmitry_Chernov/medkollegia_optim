<?php get_header();?>
<section id="section16" class="section16">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-md-offset-1">
                    <div class="single-location">
                        <div class="loc-icon">
                            <i class="fa fa-phone fa-2x red"></i>
                        </div>
                        <span class="loc-content">+7(495) 642-29-32</span>
                        <span class="loc-content">+7(495) 642-29-325</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-location">
                        <div class="loc-icon">
                            <i class="fa fa-envelope fa-2x red"></i>
                        </div>
                        <span class="loc-content">help@medkollegia.com - по медицинским вопросам</span>
                        <span class="loc-content">admin@medkollegia.com - по вопросам сотрудничества</span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single-location">
                        <div class="loc-icon">
                            <i class="fa fa-map-marker fa-2x red"></i>
                        </div>
                        <span class="loc-content">г. Москва</span>
                        <span class="loc-content" style="color: grey;">м. Нахимовский проспект</span>
                        <span class="loc-content">Болотниковская 36 к. 6</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php the_content(); ?>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <?php
                    if (get_field('contact_form_shortcode')):
                        echo get_field('contact_form_shortcode');
                    else:
                        echo do_shortcode('[contact-form-7 id="142" title="Контактная форма"]');
                    endif;
                    ?>
                </div>
                <div class="col-md-6 col-lg-6">
                    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=vLMPOC4--tvaLV09-SNioNGov-s8T45e&amp;width=555&amp;height=500&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
                </div>
            </div>
        </div>
</section>
<?php get_footer();