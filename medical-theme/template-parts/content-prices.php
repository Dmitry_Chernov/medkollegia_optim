<?php get_header(); ?>
	<section class="prices">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<header class="title-head">
						<h2>Цены на диагностику и лечение</h2>
						<p>Для уточнения свяжитесь с администратором: 8 (495) 642-29-32</p>
						<div class="line-heading">
							<span class="line-left"></span>
							<span class="line-middle">+</span>
							<span class="line-right"></span>
						</div>
					</header>
				</div>
				<div class="col-md-12">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>

