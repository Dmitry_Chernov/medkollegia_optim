<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Medicamp
 */

?>


<section id="section14" class="section-margine blog-list">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-lg-9">
				<div class="section-14-box">
					<?php the_post_thumbnail('full',array('class' => 'img-responsive')); ?>
					<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<div class="comments">
								<a class=""><i class="fa fa-calendar"></i> <?php the_time('H:m M Y'); ?></a>
<!--								<a class=""><i class="fa fa-user"></i> --><?php //the_author(); ?><!--</a>-->
							</div>
						</div>
					</div>
					<?php
							the_content();
							comments_template();
					?>
				</div>
			</div>
			<div class="col-md-3 col-lg-3">
				<?php get_sidebar(); ?>

			</div>
		</div>
	</div>
</section>