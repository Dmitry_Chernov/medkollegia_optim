<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Medicamp
 */

?>

<section id="slider" class="">
	<!-- Carousel -->
	<div id="main-slide" class="carousel slide" data-ride="carousel">

		<!-- Indicators -->
		<ol class="carousel-indicators visible-lg visible-md">
			<li data-target="#main-slide" data-slide-to="0" class="active"></li>
			<li data-target="#main-slide" data-slide-to="1"></li>
			<li data-target="#main-slide" data-slide-to="2"></li>
		</ol><!--/ Indicators end-->
		<!-- Carousel inner -->
		<div class="carousel-inner">
			<div class="item active" style="background-image:url(<?php echo get_field('slider_1'); ?>)">
				<div class="pattern"></div>
				<div class="slider-content text-right">
					<div class="col-md-12">
						<h2 class="slide-title lead effect2"><?php echo get_field('title_1'); ?></h2>
						<h3 class="slide-sub-title effect3"><?php echo get_field('subtitle_1'); ?></h3>
						<p class="slider-description lead effect3"><?php echo get_field('descritpion_1'); ?></p>
						<p class="effect3">
							<a href="<?php echo esc_url(home_url('/about-page'));?>" class="slider btn btn-primary">Услуги</a>
							<a href="<?php echo esc_url(home_url('/services-page'));?>" class="slider btn btn-secondary">О нас</a>
						</p>
					</div>
				</div>
			</div><!--/ Carousel item 1 end -->


			<div class="item" style="background-image:url(<?php echo get_field('slider_2'); ?>)">
				<div class="pattern"></div>
				<div class="slider-content">
					<div class="col-md-12 text-center">
						<h2 class="slide-title effect4"><?php echo get_field('title_2'); ?></h2>
						<h3 class="slide-sub-title effect5"><?php echo get_field('subtitle_2'); ?></h3>
						<p>
							<a href="<?php echo esc_url(home_url('/about-page'));?>" class="slider btn btn-primary">Услуги</a>
						</p>
					</div>
				</div>
			</div><!--/ Carousel item 2 end -->


			<div class="item" style="background-image:url(<?php echo get_field('slider_3'); ?>)">
				<div class="pattern"></div>
				<div class="slider-content text-right">
					<div class="col-md-12">
						<h2 class="slide-title effect6"><?php echo get_field('title_3'); ?></h2>
						<h3 class="slide-sub-title effect7"><?php echo get_field('subtitle_3'); ?></h3>
						<p class="slider-description lead effect7"><?php echo get_field('descritpion_3'); ?></p>
						<p>
							<a href="<?php echo esc_url(home_url('/doctors-page'));?>" class="slider btn btn-primary">Специалисты</a>
							<a href="<?php echo esc_url(home_url('/services-page'));?>" class="slider btn btn-secondary">О нас</a>
						</p>
					</div>
				</div>
			</div><!--/ Carousel item 3 end -->

		</div><!-- Carousel inner end-->

		<!-- Controllers -->
		<a class="left carousel-control" href="#main-slide" data-slide="prev">
			<span><i class="fa fa-angle-left"></i></span>
		</a>
		<a class="right carousel-control" href="#main-slide" data-slide="next">
			<span><i class="fa fa-angle-right"></i></span>
		</a>
	</div><!--/ Carousel end -->
</section> <!--carousel-->
<section id="section4" class="section-4 section-margine">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<header class="title-head">
					<h2>Услуги клиники</h2>
					<p>Медицинский центр «Медколлегия» специализируется на профилактике, ранней диагностике и лечении широкого спектра заболеваний взрослых.</p>
					<div class="line-heading">
						<span class="line-left"></span>
						<span class="line-middle">+</span>
						<span class="line-right"></span>
					</div>
				</header>
			</div>
			<div class="col-md-4 col-sm-6 right wow fadeInUp">
				<ul class="section">
					<li class="left"><i class="<?php echo get_field('serv_icon_1'); ?>"></i></li>
					<li><strong><?php echo get_field('serv_header_1'); ?></strong>
						<p><?php echo get_field('serv_text_1'); ?></p>
						<a href="<?php echo esc_url(home_url('/doctors-page')); ?>" class="btn btn-primary btn-sm">Узнать больше</a>
					</li>
				</ul>
				<ul class="section two">
					<li class="left"><i class="<?php echo get_field('serv_icon_2'); ?>"></i></li>
					<li><strong><?php echo get_field('serv_header_2'); ?></strong>
						<p><?php echo get_field('serv_text_2'); ?></p>
						<a href="<?php echo esc_url(home_url('/uzi')); ?>" class="btn btn-primary btn-sm">Узнать больше</a>
					</li>
				</ul>
			</div>
			<div class="col-md-4 hidden-sm hidden-xs left wow fadeInUp" data-wow-delay=".2s"><img alt="" src="<?php echo get_field('center_img_serv'); ?>?>/assets/images/m4.png"></div>
			<!-- end left -->
			<div class="col-md-4 col-sm-6 right wow fadeInUp" data-wow-delay=".3s">
				<ul class="section three">
					<li class="left"><i class="<?php echo get_field('serv_icon_3'); ?>"></i></li>
					<li><strong><?php echo get_field('serv_header_3'); ?></strong>
						<p><?php echo get_field('serv_text_3'); ?></p>
						<a href="<?php echo esc_url(home_url('/laboratornye-issledovaniya')); ?>" class="btn btn-primary btn-sm">Узнать больше</a>
					</li>
				</ul>
				<ul class="section four">
					<li class="left"><i class="<?php echo get_field('serv_icon_4'); ?>"></i></li>
					<li><strong><?php echo get_field('serv_header_4'); ?></strong>
						<p><?php echo get_field('serv_text_4'); ?></p>
						<a href="<?php echo esc_url(home_url('/holterovskoe-monitorirovanie')); ?>" class="btn btn-primary btn-sm">Узнать больше</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>   <!--service-->
<section id="section5" class="section-5 section-margine">
	<div class="container">
		<div class="row my-team">
			<div class="col-md-12">
				<header class="title-head">
					<h2>Великолепная команда профессионалов</h2>
					<p>«Медколлегия» это сообщество врачей-профессионалов своего дела, направленное на успешную диагностику и лечение заболеваний.</p>
					<div class="line-heading">
						<span class="line-left"></span>
						<span class="line-middle">+</span>
						<span class="line-right"></span>
					</div>
				</header>
			</div>
			<?php $loop = new WP_Query( array( 'post_type' => 'doctor','doc_label' => 'front_page','orderby' => 'post_id', 'order' => 'ASC' ) ); ?>
			<?php while( $loop->have_posts() ) : $loop->the_post();  ?>

				<div class="col-md-3 col-sm-6 my-team-member wow fadeInUp">
					<div class="my-member-img">
						<?php the_post_thumbnail('full',array('class' => 'img-responsive')); ?>
					</div>
					<div class="my-team-detail text-center">
						<h4 class="my-member-name"><?php echo get_field('doctor_name'); ?></h4>
						<p class="my-member-post"><?php echo get_field('position'); ?><div class="my-member-social">
							<ul>
								<li><a href="<?php echo get_field('email'); ?>" target="_blank"><i class="fa fa-envelope"></i></a></li>
								<li><a href="<?php echo get_field('facebook'); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
								<li><a href="<?php echo get_field('twitter'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
								<li><a href="<?php echo get_field('instagram'); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			<?php endwhile; wp_reset_query();?>
		</div>
	</div>
</section>  <!--proff-->
<section id="section3" class="section-margine section3-background">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6">
				<div class="section-3-box">
					<figure><i class="<?php echo get_field('stat_icon_1'); ?>"></i></figure>
					<h3><span class="counter"><?php echo get_field('stat_int_1'); ?></span>+</h3>
					<h4><?php echo get_field('stat_name_1'); ?></h4>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="section-3-box">
					<figure><i class="<?php echo get_field('stat_icon_2'); ?>"></i></figure>
					<h3><span class="counter"><?php echo get_field('stat_int_2'); ?></span>+</h3>
					<h4><?php echo get_field('stat_name_2'); ?></h4>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="section-3-box">
					<figure><i class="<?php echo get_field('stat_icon_3'); ?>"></i></figure>
					<h3><span class="counter"><?php echo get_field('stat_int_3'); ?></span>+</h3>
					<h4><?php echo get_field('stat_name_3'); ?></h4>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="section-3-box">
					<figure><i class="<?php echo get_field('stat_icon_4'); ?>"></i></figure>
					<h3><span class="counter"><?php echo get_field('stat_int_4'); ?></span>+</h3>
					<h4><?php echo get_field('stat_name_4'); ?></h4>
				</div>
			</div>
		</div>
	</div>
</section> <!--statistics-->
<section id="section2" class="section-margine">
	<div class="container">
		<div class="row">
			<div class="col-md-8 nopadding">
				<div class="col-md-6 col-sm-6">
					<div class="section-2-box-left wow fadeInLeft">
						<figure><img src="<?php echo get_field('img_article_1'); ?>" class="img-responsive"></figure>
						<h4><?php echo get_field('article_1_header'); ?></h4>
						<p><?php echo get_field('article_1_text'); ?></p>
						<a href="<?php echo get_field('article_1_link'); ?>" class="btn btn-primary">Далее</a>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="section-2-box-left wow fadeInLeft">
						<figure><img src="<?php echo get_field('img_article_2'); ?>" class="img-responsive"></figure>
						<h4><?php echo get_field('article_2_header'); ?></h4>
						<p><?php echo get_field('article_2_text'); ?></p>
						<a href="<?php echo get_field('article_2_link'); ?>" class="btn btn-primary">Далее</a>
					</div>
				</div>
			</div>
			<div class="col-md-4  col-sm-12">
				<div class="section-2-box-right wow fadeInRight">
					<h3>Часы работы</h3>
					<p class="text-justify"><?php echo get_field('working_descr'); ?></p>
					<ul>
						<li><?php echo get_field('mn_fr'); ?></li>
						<li><?php echo get_field('st_day'); ?></li>
						<li><?php echo get_field('sn_day'); ?></li>
					</ul>
					<a data-toggle="modal" href="#appointment" class="btn btn-default">Записаться</a>
				</div>
			</div>
		</div>
	</div>
</section> <!--2article+form-->
<section id="section1" class="section-margine">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6">
				<div class="section-1-box wow bounceIn">
					<div class="section-1-box-icon-background"><i class="<?php echo get_field('icon_1'); ?>"></i></div>
					<h4 class="text-center"><?php echo get_field('bf_title_1'); ?></h4>
					<p class="text-center"><?php echo get_field('bf_text_1'); ?></p>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="section-1-box wow bounceIn">
					<div class="section-1-box-icon-background"><i class="<?php echo get_field('icon_2'); ?>"></i></div>
					<h4 class="text-center"><?php echo get_field('bf_title_2'); ?></h4>
					<p class="text-center"><?php echo get_field('bf_text_2'); ?></p>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="section-1-box wow bounceIn">
					<div class="section-1-box-icon-background"><i class="<?php echo get_field('icon_3'); ?>"></i></div>
					<h4 class="text-center"><?php echo get_field('bf_title_3'); ?></h4>
					<p class="text-center"><?php echo get_field('bf_text_3'); ?></p>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="section-1-box wow bounceIn">
					<div class="section-1-box-icon-background"><i class="<?php echo get_field('icon_4'); ?>"></i></div>
					<h4 class="text-center"><?php echo get_field('bf_title_4'); ?></h4>
					<p class="text-center"><?php echo get_field('bf_text_4'); ?></p>
				</div>
			</div>
		</div>
	</div>
</section> <!--merits-->
<section id="section10" class="section-10-background">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-lg-9 pull-left">
				<div class="section-10-box-text-cont">
					<h3 class="lead">Мы уже готовы проверить твоё здоровье:&nbsp; <br><span class="red" style="color: red">  Позвони :  + 7 (495) 642 29 32</span></h3>
				</div>
			</div>
			<div class="col-md-3 col-lg-3 pull-right">
				<div class="section-10-btn-cont red"><a href="tel:+74956422932#" class="btn btn-secondary wow fadeInUp">Звонок</a></div>
			</div>
		</div>
	</div>
</section> <!--call-->
<section id="section14" class="section-margine">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<header class="title-head">
					<h2>Последние статьи</h2>
					<p>Свежие новости и статьи о здоровье</p>
					<div class="line-heading">
						<span class="line-left"></span>
						<span class="line-middle">+</span>
						<span class="line-right"></span>
					</div>
				</header>
			</div>
		</div>
		<div class="row">
			<?php
			$args = array( 'posts_per_page' => 3 );
			$query = new WP_Query( $args );
			while ( $query->have_posts() ) {
			$query->the_post(); ?>

			<div class="col-md-4 col-lg-4">
				<div class="section-14-box wow fadeInUp">
					<?php if(has_post_thumbnail()) { ?>
							<?php the_post_thumbnail('full',array('class' => 'img-responsive')); ?>
					<?php } ?>
					<h3><a href="<?php the_permalink();?>" title=""><?php the_title();?></a></h3>
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<div class="comments">
								<a class="btn btn-primary btn-sm"><?php the_time('H:m M Y'); ?></a>
								<a href="<?php the_permalink(); ?>" class="btn btn-primary btn-sm text-uppercase">Читать</a>
							</div>
						</div>
					</div>
					<p><?php the_excerpt(); ?></p>
				</div>
			</div>
			<?php }
			wp_reset_postdata(); ?>
		</div>
	</div>
</section> <!--blogs-->
<section id="section8" class="mytestimonial">
	<div class="container">
		<div class="row">

				<div data-ride="carousel" class="carousel slide" id="testimonial">
					<ol class="carousel-indicators">
						<?php $loop = new WP_Query( array( 'post_type' => 'testimonial', 'orderby' => 'post_id', 'order' => 'DESC' ) ); ?>
						<?php while( $loop->have_posts() ) : $loop->the_post(); ?>
							<li data-target="#testimonial" data-slide-to="<?php echo $loop->current_post; ?>" class="active"><img alt="Testimonial" class="img-responsive" src="<?php echo get_field('tsm_img'); ?>">
							</li>

						<?php  endwhile; wp_reset_query(); ?>
					</ol>
				<div class="carousel-inner">
					<?php $loop = new WP_Query( array( 'post_type' => 'testimonial', 'orderby' => 'post_id', 'order' => 'DESC' ) ); ?>
					<?php while( $loop->have_posts() ) : $loop->the_post();  ?>
					<div class="item text-center quotes-detail left <?php if($loop->current_post == "0"): echo " active"; else: echo " next"; endif; ?> ">
						<p class="client-quote"><i class="fa fa-quote-left"></i><?php echo get_field('tsm_text'); ?> <i class="fa fa-quote-right "></i></p>
						<h5 class="client-name"><?php echo get_field('tsm_name'); ?></h5>
					</div>
					<?php endwhile; wp_reset_query(); ?>
				</div> <!-- end carosel-inner -->

			</div> <!-- end Quotes -->
		</div>
	</div>
</section> <!--testimonial-->
<section id="section9" class="section-9-background">
	<div class="container">
		<div class="row">
			<div class="col-md-2 col-sm-4 col-xs-6"><img src="<?php echo get_field('partner');   ?>" class="img-responsive wow fadeInUp" alt=""></div>
			<div class="col-md-2 col-sm-4 col-xs-6"><img src="<?php echo get_field('partner_2'); ?>" class="img-responsive wow fadeInUp" alt=""></div>
			<div class="col-md-2 col-sm-4 col-xs-6"><img src="<?php echo get_field('partner_3'); ?>" class="img-responsive wow fadeInUp" alt=""></div>
			<div class="col-md-2 col-sm-4 col-xs-6"><img src="<?php echo get_field('partner_4'); ?>" class="img-responsive wow fadeInUp" alt=""></div>
			<div class="col-md-2 col-sm-4 col-xs-6"><img src="<?php echo get_field('partner_5'); ?>" class="img-responsive wow fadeInUp" alt=""></div>
			<div class="col-md-2 col-sm-4 col-xs-6"><img src="<?php echo get_field('partner_6'); ?>" class="img-responsive wow fadeInUp" alt=""></div>
		</div>
	</div>
</section> <!--partners-->
<section id="section23" class="appointment">
	<div class="modal fade" id="appointment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog style-one" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Запишитесь на прием</h4>
				</div>
				<div class="modal-body">
					<div class="appoinment-form-outer">
						<?php echo the_field('contact_form'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> <!--modalFormAppoint-->
