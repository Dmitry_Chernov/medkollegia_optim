<?php get_header(); ?>
    <section id="section-12">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-md-12">
                    <div class="portfolioFilter text-center">
                        <a href="<?php home_url();?>" data-filter="*" class="current">All Categories</a>/
                        <a href="<?php home_url();?>" data-filter=".acupuncture">Acupuncture</a>/
                        <a href="<?php home_url();?>" data-filter=".lungs">Lungs</a>/
                        <a href="<?php home_url();?>" data-filter=".surgery">Surgery</a>/
                        <a href="<?php home_url();?>" data-filter=".medicine">Medicine</a>
                    </div>
<!--                    <div class="portfolioContainer">-->
<!--                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 medicine text-center"> <a class="magnific-popup" href="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/1.jpg"><img src="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/1.jpg" class="img-responsive wow zoomIn" alt="image"></a> </div>-->
<!--                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 acupuncture lungs text-center"> <a class="magnific-popup" href="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/2.jpg"><img src="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/2.jpg" class="img-responsive wow zoomIn" alt="image"></a> </div>-->
<!--                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 surgery text-center"><a class="magnific-popup" href="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/3.jpg"> <img src="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/3.jpg" alt="image" class="img-responsive wow zoomIn"></a> </div>-->
<!--                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 acupuncture lungs text-center"><a class="magnific-popup" href="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/4.jpg"> <img src="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/4.jpg" alt="image" class="img-responsive wow zoomIn"></a> </div>-->
<!--                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 places medicine text-center"><a class="magnific-popup" href="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/5.jpg"> <img src="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/5.jpg" alt="image" class="img-responsive wow zoomIn"></a> </div>-->
<!--                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 acupuncture lungs medicine text-center"><a class="magnific-popup" href="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/6.jpg"> <img src="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/6.jpg" alt="image" class="img-responsive wow zoomIn"></a> </div>-->
<!--                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 surgery medicine text-center"><a class="magnific-popup" href="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/7.jpg"> <img src="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/7.jpg" alt="image" class="img-responsive wow zoomIn"></a> </div>-->
<!--                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 surgery text-center"><a class="magnific-popup" href="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/8.jpg"> <img src="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/8.jpg" alt="image" class="img-responsive wow zoomIn"></a> </div>-->
<!--                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 acupuncture food medicine text-center"><a class="magnific-popup" href="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/9.jpg"> <img src="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/9.jpg" alt="image" class="img-responsive wow zoomIn"></a> </div>-->
<!--                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 surgery medicine text-center"><a class="magnific-popup" href="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/1.jpg"> <img src="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/7.jpg" alt="image" class="img-responsive wow zoomIn"></a> </div>-->
<!--                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 surgery text-center"><a class="magnific-popup" href="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/2.jpg"> <img src="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/8.jpg" alt="image" class="img-responsive wow zoomIn"></a> </div>-->
<!--                        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 acupuncture food medicine text-center"><a class="magnific-popup" href="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/3.jpg"> <img src="--><?php //echo get_template_directory_uri();?><!--/assets/images/portfolio/9.jpg" alt="image" class="img-responsive wow zoomIn"></a> </div>-->
<!--                    </div>-->
                </div>
            </div>
        </div>
    </section>
    <script src="<?php get_template_directory_uri(); ?>/assets/js/jquery.magnific-popup.min.js"></script>
<?php get_footer();
