<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Medicamp
 */

?>

<section  class="section-5 section-margine">
	<div class="container">
		<div class="row my-team">
			<div class="col-md-12">
				<header class="title-head">
					<h2>Великолепная команда профессионалов</h2>
					<p>«Медколлегия» это сообщество врачей-профессионалов своего дела, направленное на успешную диагностику и лечение заболеваний.</p>
					<div class="line-heading">
						<span class="line-left"></span>
						<span class="line-middle">+</span>
						<span class="line-right"></span>
					</div>
				</header>
			</div>
			<?php $loop = new WP_Query( array( 'post_type' => 'doctor','posts_per_page' => 30, 'orderby' => 'post_id','order' => 'ASC' ) );  ?>
			<?php while(   $loop->have_posts() ) : $loop->the_post();   ?>
			<div class="col-md-12 col-sm-12 clearfix doctor-single">
				<div class="col-md-3 col-xs-6  my-team-member wow fadeInUp">
					<div class="my-member-img">
						<?php the_post_thumbnail('full',array('class' => 'img-responsive')); ?>
					</div>
					<div class="my-team-detail text-center">
						<h4 class="my-member-name"><?php echo get_field('doctor_name'); ?></h4>
						<p class="my-member-post"><?php echo get_field('position'); ?></p>
						<div class="my-member-social">
							<ul>
								<li><a href="<?php echo get_field('email'); ?>" target="_blank"><i class="fa fa-envelope"></i></a></li>
								<li><a href="<?php echo get_field('facebook'); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
								<li><a href="<?php echo get_field('twitter'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
								<li><a href="<?php echo get_field('instagram'); ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-9 col-sm-6 col-xs-6 wow fadeInDown">
					<h4 class="red"><?php echo get_field('full_name'); ?></h4>
					<p><?php the_content(); ?></p>
				</div>
			</div>
			<?php endwhile; wp_reset_postdata(); ?>
		</div>
	</div>
</section>

