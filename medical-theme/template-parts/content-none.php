<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Medicamp
 */

get_header(); ?>
<section id="section19" class="section19">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-12"><p><?php get_search_form(); ?></p></div>
			<div class="col-md-12 col-lg-12"><h3><?php esc_html_e( 'Nothing Found', 'medical-theme' ); ?></h3></div>
			<div class="col-md-12 col-lg-12">
				<div class=" text-center">
					<a href="<? esc_url(home_url()); ?>" class="btn btn-primary">Домой</a>
					<a "<? esc_url(home_url('/contacts-page')); ?>"  class="btn btn-primary">Сообщить о порблеме</a>
				</div>
			</div>

		</div>
	</div>
</section>
</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();




