<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Medicamp
 */

?>

<div class="section-14-box">
	<?php the_post_thumbnail('full',array('class' => 'img-responsive')); ?>
	<h3 class="text-left"><a href="<?php the_permalink();?>" title=""><?php the_title();?></a></h3>
	<div class="row">
		<div class="col-md-12 col-lg-12">
			<div class="comments">
				<a class=""><i class="fa fa-calendar"></i> <?php the_time('H:m M Y'); ?>/</a>
				<a class=""><i class="fa fa-download"></i>  <?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></a>
<!--				<a class=""><i class="fa fa-user"></i> --><?php //the_author(); ?><!--</a>-->
			</div>
		</div>
	</div>
	<p><?php the_excerpt(); ?></p>
	<div class="row">
		<div class="col-md-12 col-lg-12">
			<div class="text-left"><a href="<?php the_permalink(); ?>" class="btn btn-primary">Подробнее</a></div>
		</div>
	</div>
</div>




