<?php get_header(); ?>
    <section id="section20" class="section20 blog-list">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-lg-9">
                <?php $loop = new WP_Query( array( 'post_type' => 'question', 'orderby' => 'post_id', 'order' => 'DESC' ) ); ?>
                <?php while( $loop->have_posts() ) : $loop->the_post(); ?>
                    <div class="section-20-box">
                        <div class="section-20-box-icon-cont"><i class="fa fa-question-circle fa-2x"></i></div>
                        <div class="section-20-box-text-cont">
                            <h5><?php the_title(); ?></h5>
                            <p><?php the_content(); ?></p>
                        </div>
                    </div>
                <?php endwhile; wp_reset_query(); ?>
                </div>
                <div class="col-md-3 col-lg-3">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </section>
<?php

get_footer();