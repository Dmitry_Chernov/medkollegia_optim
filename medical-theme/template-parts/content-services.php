<?php get_header(); ?>

<section id="section4" class="section-margine">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="section-4-box wow fadeIn">
                    <div class="section-4-box-icon-cont"><i class="<?php echo get_field('serv_icon_1'); ?>"></i></div>
                    <div class="section-4-box-text-cont">
                        <h5><?php echo get_field('serv_header_1'); ?></h5>
                        <p><?php echo get_field('serv_desc_1'); ?></p>
                    </div>
                </div>
                <div class="section-4-box wow fadeIn" data-wow-delay=".1s">
                    <div class="section-4-box-icon-cont"><i class="<?php echo get_field('serv_icon_2'); ?>"></i></div>
                    <div class="section-4-box-text-cont">
                        <h5><?php echo get_field('serv_header_2'); ?></h5>
                        <p><?php echo get_field('serv_desc_2'); ?></p>
                    </div>
                </div>
                <div class="section-4-box wow fadeIn" data-wow-delay=".2s">
                    <div class="section-4-box-icon-cont"><i class="<?php echo get_field('serv_icon_3'); ?>"></i></div>
                    <div class="section-4-box-text-cont">
                        <h5><?php echo get_field('serv_header_3'); ?></h5>
                        <p><?php echo get_field('serv_desc_3'); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4"><img src="<?php echo get_field('serv_doc_img'); ?>" class="img-responsive wow zoomIn" alt=""></div>
            <div class="col-md-4 col-lg-4">
                <div class="section-4-box wow fadeIn">
                    <div class="section-4-box-icon-cont"><i class="<?php echo get_field('serv_icon_4'); ?>"></i></div>
                    <div class="section-4-box-text-cont">
                        <h5><?php echo get_field('serv_header_4'); ?></h5>
                        <p><?php echo get_field('serv_desc_4'); ?></p>
                    </div>
                </div>
                <div class="section-4-box wow fadeIn" data-wow-delay=".1s">
                    <div class="section-4-box-icon-cont"><i class="<?php echo get_field('serv_icon_5'); ?>"></i></div>
                    <div class="section-4-box-text-cont">
                        <h5><?php echo get_field('serv_header_5'); ?></h5>
                        <p><?php echo get_field('serv_desc_5'); ?></p>
                    </div>
                </div>
                <div class="section-4-box wow fadeIn" data-wow-delay=".2s">
                    <div class="section-4-box-icon-cont"><i class="<?php echo get_field('serv_icon_6'); ?>"></i></div>
                    <div class="section-4-box-text-cont">
                        <h5><?php echo get_field('serv_header_6'); ?></h5>
                        <p><?php echo get_field('serv_desc_6'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="section22" class="section22">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-lg-7">
                <div class="section-22-box-text-cont">
                    <h2><?php echo get_field('serv_main_header'); ?></h2>
                    <h5><?php echo get_field('serv_main_subheader'); ?>.</h5>
                    <p><?php echo get_field('serv_main_text'); ?></p>
                </div>
            </div>
            <div class="col-md-5 col-lg-5"><div class="section-5-box-text-cont wow fadeInLeft"><img src="<?php echo get_field('serv_main_img'); ?>" class="img-responsive" alt=""></div></div>
        </div>
    </div>
</section>
<section id="section11" class="section-margine">
    <div class="container">
        <div class="row">
            <?php $loop = new WP_Query( array( 'post_type' => 'specialization', 'orderby' => 'post_id', 'order' => 'DESC' ) ); ?>
            <?php while( $loop->have_posts() ) : $loop->the_post(); ?>
            <div class="col-md-3 col-lg-3">
                <div class="section-11-box">
                    <a href="<?php if( get_field('medspec_permalink') ) { echo get_field('medspec_permalink'); } else { echo esc_url(home_url()); }?>"><div class="section-11-box-icon-background"><i class="<?php echo get_field('medspec_icon'); ?>"></i></div></a>
                    <h4 class="text-center"><?php echo get_field('medspec_header'); ?></h4>
                    <p class="text-center"><?php echo get_field('medspec_desc'); ?></p>
                </div>
            </div>
            <?php  endwhile; wp_reset_query(); ?>
        </div>
    </div>
</section>

<?php get_footer();
